#Ejemplo de Diccionario Video 9 Alfredo
#Video 8 2019/07/26
diccionario={"Alemania":"Berlín","Francia":"París","México":"CDMX","Mosqueteros":3}
print(diccionario["Francia"])
print(diccionario["México"])
print(diccionario)
#Agregar un elemento al diccionario
diccionario["Italia"]="Milan"
print(diccionario)
#Sobreescribir un elemento existente
diccionario["Italia"]="Roma"
print(diccionario)
#Eliminar elementos de un diccionario
del diccionario["Alemania"]
del diccionario["Francia"]
del diccionario["México"]
del diccionario["Mosqueteros"]
del diccionario["Italia"]
print(diccionario)
#Asignación de valores de una tupla a un diccionario
tupla=["España","Alemania","México"]
diccionario={tupla[0]:"Madrid",tupla[1]:"Berlin",tupla[2]:"CDMX"}
print(diccionario)
#almacenamiento de una tupla en un diccionario
diccionario={23:"Jordan","Nombre":"Michael","Equipo":"chicago","Anillo":{"temporadas":[1991,1992,1993,1996,1997,1998]}}
print(diccionario)
print(diccionario["Equipo"])
print(diccionario["Anillo"])
#muestra las claves del diccionario
print(diccionario.keys())
#Muestra los valores del diccionario
print(diccionario.values())
#Muestra el número de elementos del diccionario
print(len(diccionario))

