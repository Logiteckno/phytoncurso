#Ejemplo de Listas/array Video 7 Alfredo
#Video 7 2019/07/25

lista=["Alfredo","Karen",10.7,"Pepe"]
#Multiplica los elementos del array
lista2=["Juan"] *3
#imprimir todos los elementos del array
print(lista[:])
#imprimir el elemento selecccinado del array
print(lista[3])
#imprimir el elemento selecccinado de izquierda a derecha comenzando con -1 del array
print(lista[-2])
#imprimir los elementos del 0 hasta el 1 y excluye los elementos apartir del 2
print(lista[0:2])
#imprimir los elementos del indice 2 hasta el final
print(lista[2:])
#Ingresa un nuevo datos a un array
lista.append("nuevo valor")
print(lista[4])
#Agrega un nuevo valor al indice seleccionado y se reccoren los demas indices 
lista.insert(2,"valor seleccionado")
print(lista[2])
#Agrega varios elementos a un array
lista.extend(["valor ext1",27])
print(lista[:])
#Muestra el indice donde se encuentra el elemento
print(lista.index("valor ext1"))
#Muestra si existe un elemento en un array
print("No existe" in lista)
print("Alfredo" in lista)
#Elimina el elemento seleccionado del array
lista.remove("Karen")
lista.append("Agregado")
print(lista[:])
#Elimina el ultimo valor agregado al array
lista.pop()
print(lista[:])
#Concatenar listas 
lista3=lista+lista2
print(lista3[:])