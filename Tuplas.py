#Ejemplo de Tuplas Video 8 Alfredo
#Video 8 2019/07/26
print ("() Es una tupla")
print ("[] Es una lista")
# [] Es una lista
tupla=("Alfredo",13,1,1995.1,13)
#Se impreme de dos formas diferentes
#print(tupla[:])
print(tupla)
#Convertir tupla en lista 
lista=list(tupla)
#Se impre de dos formas diferentes
#print(lista)
print(lista[:])
#Convierte una lista en una tupla
tupla=tuple(lista)
print(tupla)
print("Alfredo" in tupla)
#Contabilisa el numero de elementos seleccionados
print(tupla.count(13))
#Muestra la longitud/numero de elementos
print(len(tupla))
#Tupla sin parentesis no recomendado
#tupla2="Juan",13,1
tupla2=("Juan",13,1,1995)
print(tupla2)
#Desempaquetado de tupla
nombre,dia,mes,agno=tupla2
print(nombre)
print(dia)
print(mes)
print(agno)