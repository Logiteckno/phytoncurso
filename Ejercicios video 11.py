print("Ejercicio 1:\n"+"• Crea un programa que pida dos números por teclado. El programa tendrá una función llamada “DevuelveMax” encargada de devolver el número más alto de los dos introducidos.")
#Función magica para valorar número mayor y menor
def DevuelveMax(valor1,valor2):
	#resultado=(valor1+" y "+valor2+" son iguales.")
	if valor1>valor2:
		resultado=(str(valor1)+" es más grande que "+str(valor2)+".")
		#resultado=("es más grande que ")
	elif valor1<valor2:
		resultado=(str(valor2)+" es más grande que "+str(valor1)+".")
	else:
		resultado=(str(valor1)+" y "+str(valor1)+" son iguales.")
		#resultado=resultado=(" es más grande que ")
	return(resultado)
print("Calculando numero mayor o menor")
valor1=int(input(("Ingresa el primer valor: ")))
valor2=int(input(("Ingresa el primer valor: ")))
print(DevuelveMax(valor1,valor2))
"""************************************************"""
print("\nEjercicio 2:\n"+"• Crea un programa que pida por teclado “Nombre”, “Dirección” y “Teléfono”. Esos tres datos deberán ser almacenados en una lista y mostrar en consola el mensaje: “Los datos personales son: nombre apellido teléfono” (Se mostrarán los datos introducidos por teclado).")
#Solicitando por consola los elementos a ingresar a la lista/array 
Datos=[(input("Ingresa tu nombre: ")),(input("Ingresa tu dirección: ")),(input("Ingresa tu teléfono: "))]
print(Datos)
"""************************************************"""
print("\nEjercicio 3:\n"+"• Crea un programa que pida tres números por teclado. El programa imprime en consola la media aritmética de los números introducidos.")
#Solicitando por consola los elementos a ingresar a la lista/array 
Datos=[int((input("Ingresa el primer digito: "))),int((input("Ingresa el segundo digito: "))),int((input("Ingresa el tercer digito: ")))]
#Sumando los elementos de la lista y dividiendolo por el número de elementos del array
total=Datos[0]+Datos[1]+Datos[2]
print("El promedio es: "+str(total/len(Datos)))
